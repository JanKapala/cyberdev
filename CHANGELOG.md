# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2024-02-08
### Added
- Create project repository [@JanKapala]
- Create project structure [@JanKapala]
- Set up tools and implement repo good practices [@JanKapala]
- Configure tests [@JanKapala]
- Configure CI/CD [@JanKapala]
- Create simplest Cyberdev that can do a PR based on repo content (e.g. issues) [@JanKapala]
- Add web search tool [@JanKapala]
### Changed
### Fixed
### Removed
