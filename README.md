# Cyberdev
This is a POC of the simplest automated developer that works on the given 
repository (with a very special dedication to [**Zbigniew**](https://github.com/zbigniewsobiecki) ;) 

For now, it can read issues and create PRs that resolve them. It also pay 
attention to the issues comments (PR comments and review comments aren't 
supported yet) - pretty basic, however there is an enormous space for 
improvements, e.g.:
- adding memory and ability to start from checkpoint
- adding more tools
- adding pagination of the every tool output, as well as output search tools - ability for the bot to process arbitrarily long outputs from tools
- bot taking notes ability
- creation of new tools by the bot through the automation of its past successfull runs.
- there is more, but let's KISS


### Run the Cyberdev
To run the `Cyberdev`:
- clone this repo
- follow steps from the [Setup](#setup)
- run the following command in the project root:
```bash
poetry run inv cyberdev --max-iterations=2
```

### Setup

#### Project installation
- Install [pyenv](https://github.com/pyenv/pyenv)
- Install Python 3.11.5 (via pyenv):
  - You may need to run `sudo apt install -y python3.11-dev python3.11-tk liblzma-dev libsqlite3-dev` before
- Set python version: `pyenv local 3.11.5`
- Install [Poetry](https://python-poetry.org/docs/#installing-with-the-official-installer)
- Install project dependencies: `poetry install`

#### Working Repo
Prepare a new repository in the Github and add some issues there. This 
repository will be the workspace of the Cyberdev. It will try to solve issues 
in this repo.

For the demonstration purpose, following repository has been created: https://github.com/JanKapala/exemplary_repo

`Deploy` job will run Cyberdev on this repo. By default, there are only 2 iterations of the Cyberdev works steps but it can be overwritten during manual run of the `Deploy` job in the Gitlab CI/CD pipeline Web UI.

#### Github App
- Create github app as described [here](https://docs.github.com/en/apps/creating-github-apps/registering-a-github-app/registering-a-github-app)
- Give this app access to the repository in which the Cyberdev will work.

#### ENV variables
Set following ENV vars (you can create `.env` file in the project root and set ENV vars there):
- `OPENAI_API_KEY`: for the LLM (brain of the Cyberdev)
- `TAVILY_API_KEY`: for the search tool that can be used by the Cyberdev 
- `GITHUB_APP_ID`: Id of the application on behalf of which the bot will act in the repository
- `GITHUB_APP_PRIVATE_KEY`: pretty self-explaining
- `GITHUB_REPOSITORY`: string in the following form: `<github_user_name>/<github_repo_name>`
- `GITHUB_BRANCH`: Cyberdev will add features on this branch.
- `GITHUB_BASE_BRANCH`: base branch of the repo.
- `MAX_ITERATIONS`: Default max iterations of the cyberdev work (used when call `inv cyberdev` without argument)

### Local Development
- Install [pre-commit](https://pre-commit.com/#install) on the system
- Install pre-commit for the project with: `pre-commit install` in the project root dir
- Install [PyCharm](https://www.jetbrains.com/pycharm/)
- This project uses Google style docstrings, set it in the Pycharm settings | Tools | Python Integrated Tools and also check following checkboxes:
  - Analyze Python code in docstrings
  - Render external documentation for stdlib

#### For local tests of gitlab CI/CD (optional)
- Install and register gitlab runner 
  - Set "Run untagged jobs" in the gitlab CI -> runners -> previously setup runner
  - Set concurrent = 30 in the /etc/gitlab-runner/config.toml
