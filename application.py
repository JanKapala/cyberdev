import sys

from dotenv import load_dotenv

from cyberdev.poc import create_agent_executor, get_guideline
from cyberdev.utils.custom_logging import init_logging, LOGGER

if __name__ == "__main__":
    load_dotenv()
    init_logging()

    if len(sys.argv) != 2:
        raise ValueError("Invalid number of arguments, call like "
                         "this: python application.py <max_iterations>")
    max_iterations = int(sys.argv[1])

    agent_executor = create_agent_executor(max_iterations)
    guideline = get_guideline()

    LOGGER.info("Starting agent executor...")
    agent_executor.invoke({"input": guideline})

