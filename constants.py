"""All project global constants."""

import os

PROJECT_ROOT_PATH = os.path.dirname(os.path.abspath(__file__))

SOFTWARE_DEVELOPMENT_GUIDELINE_PATH = os.path.join(
    PROJECT_ROOT_PATH, "cyberdev", "software_development_guideline.md"
)
LOGS_DIR_PATH = os.path.join(PROJECT_ROOT_PATH, "logs")
DOT_ENV_PATH = os.path.join(PROJECT_ROOT_PATH, ".env")
