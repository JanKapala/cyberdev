"""POC of the simplest automated developer: Cyberdev
(cool names start with "cyber" these days)"""

import os

from langchain import hub
from langchain.agents import AgentExecutor, create_react_agent
from langchain_community.agent_toolkits.github.toolkit import GitHubToolkit
from langchain_community.tools.tavily_search import TavilySearchResults
from langchain_community.utilities.github import GitHubAPIWrapper
from langchain_core.tools import tool
from langchain_experimental.tools import PythonREPLTool
from langchain_openai import ChatOpenAI

from constants import SOFTWARE_DEVELOPMENT_GUIDELINE_PATH
from cyberdev.utils.custom_logging import LOGGER


@tool
def ask_boss(question: str) -> str:
    """Ask the boss a question if you really don't know what to do.

    Args:
        question: Question to the boss

    Returns:
        Boss response.
    """

    boss_response = input(f"Here is the question from the Cyberdev: {question}")
    return boss_response


MODEL_NAME = "gpt-4-0125-preview"
PROMPT_NAME = "hwchase17/react"


def create_agent_executor(max_iterations: int) -> AgentExecutor:
    """Creates an agent executor chain that can be invoked with
    the `.invoke({"input": "<placeholder>"})`.

    Args:
        max_iterations: Max iterations of the Cyberdev work steps.

    Returns:
        Configured agent executor chain.
    """

    LOGGER.info("Creating LLM...")
    llm = ChatOpenAI(temperature=0, model=MODEL_NAME)
    LOGGER.info("LLM created!")

    LOGGER.info("Creating tools...")
    github = GitHubAPIWrapper(  # type: ignore[call-arg]
        # workaround for gitlab min variable length 8 (two preceding zeros)
        github_app_id=str(int(os.environ["GITHUB_APP_ID"]))
    )
    toolkit = GitHubToolkit.from_github_api_wrapper(github)
    search = TavilySearchResults()
    tools = toolkit.get_tools() + [search] + [ask_boss] + [PythonREPLTool()]
    LOGGER.info("Tools created!")

    LOGGER.info("Downloading agent prompt...")
    prompt = hub.pull(PROMPT_NAME)
    LOGGER.info("Agent prompt downloaded!")

    LOGGER.info("Creating agent...")
    agent = create_react_agent(
        llm=llm,
        tools=tools,  # type: ignore[arg-type]
        prompt=prompt,
    )
    LOGGER.info("Agent created!")

    LOGGER.info("Creating agent executor...")
    agent_executor = AgentExecutor(
        agent=agent,  # type: ignore[arg-type]
        tools=tools,  # type: ignore[arg-type]
        verbose=True,
        return_intermediate_steps=True,
        handle_parsing_errors=True,
        max_iterations=max_iterations,
    )
    LOGGER.info("Agent executor created!")

    return agent_executor


def get_guideline() -> str:
    """Get guideline needed by the Cyberdev to know how to operate.

    Returns:
        Software development guideline.
    """
    with open(
        SOFTWARE_DEVELOPMENT_GUIDELINE_PATH, "r", encoding="UTF-8"
    ) as file:
        return file.read()
