# pylint: disable=fixme
"""Project logging configuration."""

import logging
import os
from typing import Optional

from constants import LOGS_DIR_PATH

_CYBERDEV_LOGGER_NAME = "CYBERDEV_LOGGER_NAME"
LOGGER = logging.getLogger(_CYBERDEV_LOGGER_NAME)

# TODO: better logging, json/human-readable flag, colors when human readable,
#  underlying libraries logs flag.


def init_logging(
    level: int = logging.INFO, format_string: Optional[str] = None
) -> None:
    """Configure logging for the process it is executed in.

    Args:
        level: Min log level at which and above logs will be visible.
        format_string:

    Returns: None.

    """
    format_string = (
        format_string
        or "[%(asctime)s][%(name)s][%(levelname)s][%(pathname)s][%(funcName)s]"
        "[%(lineno)d][%(process)d] %(message)s"
    )
    log_file_path = os.path.join(LOGS_DIR_PATH, "main_logs.txt")

    logger = logging.getLogger(_CYBERDEV_LOGGER_NAME)
    logger.setLevel(level)

    console_handler = logging.StreamHandler()
    file_handler = logging.FileHandler(log_file_path, mode="a", delay=True)

    # Set levels for handlers
    console_handler.setLevel(level)
    file_handler.setLevel(level)

    formatter = logging.Formatter(format_string)

    console_handler.setFormatter(formatter)
    file_handler.setFormatter(formatter)

    # Add handlers to the logger
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)
    logging.basicConfig(level=level, handlers=[console_handler, file_handler])
    logger.propagate = False
