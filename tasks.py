# pylint: disable=missing-function-docstring

"""Project tasks that can be run via command line using inv command."""
import os
from typing import Optional

from dotenv import load_dotenv
from invoke import Context, task  # type: ignore[attr-defined]

from constants import PROJECT_ROOT_PATH

SOURCES_PATHS = " ".join(
    [
        os.path.join(PROJECT_ROOT_PATH, "cyberdev"),
    ]
)
TESTS_PATH = os.path.join(PROJECT_ROOT_PATH, "tests")
UNIT_TESTS_PATH = os.path.join(TESTS_PATH, "unit")
COMPONENT_TESTS_PATH = os.path.join(TESTS_PATH, "component")


@task
def test(c: Context, run_mode: str) -> None:
    if run_mode == "coverage":
        c.run(
            f"poetry run pytest "
            f"-n auto "
            f"--randomly-seed=1234 "
            f"--cov-report term-missing:skip-covered "
            f"--cov={SOURCES_PATHS} {UNIT_TESTS_PATH} {COMPONENT_TESTS_PATH}"
        )
    else:
        specific_tests_path = os.path.join(TESTS_PATH, run_mode)
        c.run(
            f"poetry run pytest -n auto --randomly-seed=1234 "
            f"{specific_tests_path}"
        )


@task
def mypy(c: Context) -> None:
    c.run(
        f"poetry run mypy --install-types --non-interactive {SOURCES_PATHS} "
        f"{TESTS_PATH} constants.py tasks.py"
    )


@task
def black(c: Context, only_check: bool = False) -> None:
    if only_check:
        command = (
            f"poetry run black --check {SOURCES_PATHS} {TESTS_PATH} "
            f"constants.py tasks.py"
        )
    else:
        command = (
            f"poetry run black {SOURCES_PATHS} {TESTS_PATH} "
            f"constants.py tasks.py"
        )
    c.run(command)


@task
def isort(c: Context, only_check: bool = False) -> None:
    if only_check:
        command = (
            f"poetry run isort --check {SOURCES_PATHS} {TESTS_PATH} "
            f"constants.py tasks.py"
        )
    else:
        command = (
            f"poetry run isort {SOURCES_PATHS} {TESTS_PATH} "
            f"constants.py tasks.py"
        )
    c.run(command)


@task
def lint(c: Context) -> None:
    c.run(
        f"poetry run pylint --jobs=0 --recursive=y {SOURCES_PATHS} "
        f"{TESTS_PATH} constants.py tasks.py"
    )


@task
def bandit(c: Context) -> None:
    c.run(f"poetry run bandit -c pyproject.toml -r {SOURCES_PATHS}")


@task
def cyberdev(c: Context, max_iterations: Optional[str] = None) -> None:
    load_dotenv()
    _max_iterations = max_iterations or os.environ["MAX_ITERATIONS"]
    c.run(f"poetry run python application.py {_max_iterations}")
