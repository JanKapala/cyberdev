# pylint: disable=fixme, missing-module-docstring, missing-function-docstring

import pytest
from dotenv import load_dotenv


# TODO: better handling of the test env vars
@pytest.fixture(autouse=True, scope="module")
def set_test_env_vars() -> None:
    load_dotenv()
