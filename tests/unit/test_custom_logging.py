# pylint: disable=fixme, missing-module-docstring, missing-function-docstring
# mypy: disable-error-code="no-untyped-def"

from cyberdev.utils.custom_logging import (
    _CYBERDEV_LOGGER_NAME,
    LOGGER,
    init_logging,
)

# TODO: add more tests


def test_custom_log_contains_message(capsys) -> None:
    init_logging()
    message = "placeholder"
    LOGGER.info(message)
    captured = capsys.readouterr().err
    assert message in captured


def test_custom_log_contains_cyberdev_logger_name(capsys) -> None:
    init_logging()
    message = "placeholder"
    LOGGER.info(message)
    captured = capsys.readouterr().err
    assert _CYBERDEV_LOGGER_NAME in captured
