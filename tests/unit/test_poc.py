# pylint: disable=missing-module-docstring, missing-function-docstring

from cyberdev.poc import ask_boss, create_agent_executor, get_guideline


def test_ask_boss(monkeypatch) -> None:  # type: ignore[no-untyped-def]
    response = "placeholder_response"
    monkeypatch.setattr("builtins.input", lambda _: response)
    assert ask_boss("placeholder_question") == response


def test_create_agent_executor() -> None:
    create_agent_executor(2)


def test_get_guideline() -> None:
    guideline = get_guideline()
    assert isinstance(guideline, str)
    assert len(guideline) > 100
